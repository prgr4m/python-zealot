from setuptools import setup, find_packages

setup(
    name='zealot',
    version='0.1',
    description='',
    author='John Boisselle',
    author_email='john@johnboisselle.com',
    url='https://bitbucket.org/prgr4m/zealot',
    packages=find_packages(),
    install_requires=[
        'tzlocal',
        'requests',
    ],
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Topic :: Utilities',
        'Topic :: Software Development :: Documentation'
    ],
    entry_points='''
        [console_scripts]
        zealot=zealot.main:main
    '''
)
