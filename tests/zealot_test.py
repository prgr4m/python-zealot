#!/usr/bin/env python
# -*- coding: utf-8 -*-
from os import path
import shutil
import tempfile
from vcr_unittest import VCRTestCase
import unittest
from zealot import data_dirname, zeal_docset_dir
from zealot import main as zm


class ZealotTestCase(VCRTestCase):
    def setUp(self):
        self.user_prefix = tempfile.mkdtemp()  # used only for py2 compat
        self.app_root = path.join(self.user_prefix, data_dirname)

    def tearDown(self):
        shutil.rmtree(self.user_prefix)  # used only for py2 compat

    @unittest.skip("already works")
    def test_app_setup(self):
        zm.app_setup(app_dir=self.user_prefix)
        self.assertTrue(path.exists(self.app_root),
                        "app_init didn't create data directory")
        # testing to see if json file exists
        zealot_data_file = path.join(self.app_root, 'zealot.json')
        self.assertTrue(path.exists(zealot_data_file),
                        "app_init didn't create zealot.json file")

    @unittest.skip("already works")
    def test_set_cdn_prefix(self):
        test_location_1 = 'America/New_York'
        self.assertEqual('newyork', zm.set_cdn_prefix(test_location_1),
                         "set_cdn_prefix didn't set the expected server")
        test_location_2 = 'America/Anchorage'
        self.assertEqual('sanfrancisco', zm.set_cdn_prefix(test_location_2),
                         "set_cdn_prefix didn't set the expected server")
        test_location_3 = 'Asia/Tokyo'
        self.assertEqual('london', zm.set_cdn_prefix(test_location_3),
                         "set_cdn_prefix didn't set the expected server")

    @unittest.skip("already works")
    def test_docset_index_check(self):
        self.__setup_working_env(run_index_check=True)
        index_location = path.join(self.app_root, 'index.json')
        self.assertTrue(path.exists(index_location),
                        "docset_index_check didn't download usercontrib index")

    @unittest.skip("already works")
    def test_get_docset_index_contents(self):
        self.__setup_working_env(run_index_check=True)
        content = zm.get_docset_index_contents()
        self.assertTrue(len(content) > 0,
                        "error in stripping out irrelevant data from index")

    @unittest.skip("already works")
    def test_fetch_docset(self):
        self.__setup_working_env()
        docset_name, docset_archive = self.__sample_docset_info()
        zm.fetch_docset(docset_name, docset_archive)
        target_file = zm.get_local_archive_path(docset_archive)
        self.assertTrue(path.exists(target_file),
                        "docset was not fetched/stored in expected directory")

    @unittest.skip("already works")
    def test_unpackage_docset(self):
        self.__setup_working_env()
        docset_name, docset_archive = self.__sample_docset_info()
        zm.fetch_docset(docset_name, docset_archive)
        zm.unpackage_docset(docset_archive)
        docset_location = path.join(self.app_root, 'contrib_docsets',
                                    'PyInstaller.docset')
        self.assertTrue(path.exists(docset_location),
                        "extracted docset was not found at expected location")
        archive_location = path.join(self.app_root, docset_archive)
        self.assertFalse(path.exists(archive_location),
                         "original archive was not cleaned up on disk")

    @unittest.skip("already works")
    def test_install_docset(self):
        self.__setup_working_env(run_index_check=True)
        zm.install_docset('pyinstaller')
        data_manager = zm.ZealotDM()
        self.assertTrue('PyInstaller' in data_manager,
                        "installation of docset was not stored as expected")
        test_target_path = path.join(self.app_root, 'contrib_docsets',
                                     'PyInstaller.docset')
        test_target_symlink = path.join(zeal_docset_dir,
                                        'PyInstaller.docset')
        self.assertTrue(path.exists(test_target_path, 'PyInstaller.docset'),
                        "expected docset was not extracted as expected")
        self.assertTrue(path.exists(test_target_symlink),
                        "symlink was not created as expected")

    @unittest.skip("already works")
    def test_remove_docset(self):
        self.__setup_working_env(run_index_check=True)
        zm.install_docset('pyinstaller')
        zm.remove_docset('pyinstaller')
        data_manager = zm.ZealotDM()
        self.assertFalse('PyInstaller' in data_manager,
                         "docset should not be in data manager after removal")
        test_symlink = path.join(zeal_docset_dir, 'PyInstaller.docset')
        self.assertFalse(path.exists(test_symlink),
                         "symlink should be removed when removing the docset")

    def __setup_working_env(self, run_index_check=False):
        zm.app_setup(app_dir=self.user_prefix)
        if run_index_check:
            zm.docset_index_check()

    def __sample_docset_info(self):
        return ('PyInstaller', 'PyInstaller.tgz')

if __name__ == "__main__":
    unittest.main()
