# -*- coding: utf-8 -*-
import sys
from os import path

user_prefix = '~user' if sys.platform.startswith('win') else '~'
user_dir = path.expanduser(user_prefix)
data_dirname = 'zealot' if sys.platform.startswith('win') else '.zealot'

if sys.platform.startswith('win'):  # windows
    zeal_docset_dir = ''  # need path on windows installation
else:  # defaulting to linux
    zeal_docset_dir = path.join(user_dir, '.local', 'share', 'Zeal', 'Zeal',
                                'docsets')
