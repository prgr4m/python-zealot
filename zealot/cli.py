#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

prog_desc = "Zealot: A user contrib docset management tool for zeal " \
    + "(http://zealdocs.org)"

parser = argparse.ArgumentParser(prog='zealot', description=prog_desc)
subparsers = parser.add_subparsers(title='sub-commands',
                                   dest='subparser_name',
                                   help='sub-command help')

list_parser = subparsers.add_parser('list', help="list known docsets")
list_parser.add_argument('-i', '--installed', action='store_true',
                         help='list only installed docsets')

install_help = "remove a known docset"
install_parser = subparsers.add_parser('install', help=install_help)

remove_help = "remove a docset installed by this tool"
remove_parser = subparsers.add_parser('remove', help=remove_help)

root_name = "name of the docset to be"
name_help = {
    install_parser: root_name + ' installed',
    remove_parser: root_name + ' removed'
}

for sub_parser in (install_parser, remove_parser):
    sub_parser.add_argument('name', help=name_help[sub_parser])
