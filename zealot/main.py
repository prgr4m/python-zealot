#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import datetime
import json
import os
from os import path
import shutil
import sys
import tarfile
import requests
from . import user_dir, data_dirname, zeal_docset_dir
from .cli import parser

# module level variables and classes
# =============================================================================
app_root, zealot_data, contrib_index = None, None, None
docset_dir, server_mirror = None, None


class ZealotDM(object):  # py2 compat
    """Zealot Data Manager"""
    def __init__(self):
        self._data = json.load(open(zealot_data, 'r'))

    def __del__(self):
        with open(zealot_data, 'w') as f:
            json.dump(self._data, f)

    def __contains__(self, docset_name):
        return docset_name in self._data

    def __len__(self):
        return len(self._data)

    def get_installed_docsets(self):
        return self._data

    def get_docset_names(self):
        return [k.lower() for k in self._data.keys()]

    def get_docset_info(self, docset_name):
        docset_name = docset_name.lower()
        return self._data[docset_name]

    def add_docset(self, docset_name, contrib_path, version):
        docset_name = docset_name.lower()
        if docset_name not in self._data:
            self._data[docset_name] = {
                'path': path.join(docset_dir, contrib_path),
                'version': version
            }

    def remove_docset(self, docset_name):
        docset_name = docset_name.lower()
        if docset_name in self._data:
            self._data.pop(docset_name)


# bootstrapping
# =============================================================================
def app_init():
    app_setup()
    docset_index_check()


def app_setup(app_dir=user_dir):
    global app_root, zealot_data, contrib_index, docset_dir, server_mirror
    target_dir = path.join(app_dir, data_dirname)
    target_data = path.join(target_dir, 'zealot.json')
    target_docset_dir = path.join(target_dir, 'contrib_docsets')
    if not path.exists(target_dir):
        print("[init]: first time running application")
        print("[init]: creating data directory at: {}".format(target_dir))
        os.makedirs(target_docset_dir)
    app_root = target_dir
    if not path.exists(target_docset_dir):  # data_dir can exist w/o docsets
        os.mkdir(target_docset_dir)
    if not path.exists(target_data):
        with open(target_data, 'w') as data:
            json.dump(dict(), data)
    try:  # checking to see if json file is actually valid
        json.load(open(target_data, 'r'))
    except ValueError:
        with open(target_data, 'w') as data:
            json.dump(dict(), data)
    zealot_data = target_data
    docset_dir = target_docset_dir
    server_mirror = set_cdn_prefix()
    contrib_index = path.join(target_dir, 'index.json')


def set_cdn_prefix(where_am_i=None):
    # I am in EST -- if you want something fancy feel free to hack away
    if where_am_i is None:
        from tzlocal import get_localzone
        where_am_i = get_localzone().zone
    newyork = {'America/New_York', 'America/Chicago'}
    sanfran = {'America/Denver', 'America/Phoenix', 'America/Los_Angeles',
               'America/Anchorage', 'America/Adak', 'Pacific/Honolulu'}
    if where_am_i in newyork:
        server_loc = 'newyork'
    elif where_am_i in sanfran:
        server_loc = 'sanfrancisco'
    else:  # if you're not in the US, sorry -- I'm bouncing you to london
        server_loc = 'london'
    return server_loc


def docset_index_check():
    def fetch_user_contrib_index():
        url = get_url_schema(get_index=True).format(mirror=server_mirror)
        r = requests.get(url)
        with open(contrib_index, 'wb') as new_index:
            new_index.write(r.content)

    if not path.exists(contrib_index):
        print("zeal user contrib index doesn't exist -- fetching index")
        fetch_user_contrib_index()
    else:  # check if the index is stale
        mod_time = datetime.datetime.fromtimestamp(
            path.getctime(contrib_index))
        now = datetime.datetime.now()
        stale_content = now - datetime.timedelta(days=1)
        if stale_content >= mod_time:
            print("zeal user contrib index is stale -- fetching new index")
            fetch_user_contrib_index()


# utilities
# =============================================================================
def get_url_schema(get_index=False):
    root_url = "http://{mirror}.kapeli.com/feeds/zzz/user_contributed/build/"
    if get_index:
        return root_url + "index.json"
    else:
        return root_url + "{key_name}/{archive_name}"


def get_docset_index_contents():
    zeal_index_loc = path.join(app_root, 'index.json')
    zeal_index = json.load(open(zeal_index_loc, 'r'))['docsets']
    return {k: {'archive': v['archive'], 'version': v['version']}
            for k, v in zeal_index.items()}


def get_local_archive_path(archive_name):
    return path.join(app_root, archive_name)


def print_header(title):
    print()
    print(title)
    print("=" * 80)


def fetch_docset(docset_name, docset_info):
    local_archive = get_local_archive_path(docset_info['archive'])
    url_schema = get_url_schema()
    print("[info]: fetching {} docset".format(docset_name.lower()))
    r = requests.get(url_schema.format(mirror=server_mirror,
                                       key_name=docset_name,
                                       archive_name=docset_info['archive']))
    with open(local_archive, 'wb') as docset:
        docset.write(r.content)


def unpackage_docset(docset_info):
    local_archive = get_local_archive_path(docset_info['archive'])
    docset_contrib_dirname = None
    if not path.exists(local_archive):
        print("[error]: {} not found".format(docset_info['archive']))
        sys.exit(1)  # need to look up err_code -- placeholder for now
    with tarfile.open(local_archive, 'r:gz') as docset:
        docset.extractall(path=docset_dir)
        docset_contrib_dirname = docset.getnames()[0]
    os.remove(local_archive)  # cleaning up directory after extraction
    return docset_contrib_dirname


def create_symlink(docset_dirname):
    pyinfo, os_type = sys.version_info, sys.platform
    docset_location = path.join(docset_dir, docset_dirname)
    if os_type.startswith('win'):
        if pyinfo.major == 3 and pyinfo.minor >= 2:
            os.symlink(docset_location,
                       path.join(zeal_docset_dir, docset_dirname),
                       target_is_directory=True)
        else:
            from subprocess import call
            call(['mklink', '/D', docset_location, zeal_docset_dir])
    else:
        os.symlink(docset_location,
                   path.join(zeal_docset_dir, docset_dirname),
                   target_is_directory=True)


def remove_symlink(docset_path_info):
    docset_dirname = path.basename(docset_path_info)
    target_path = path.join(zeal_docset_dir, docset_dirname)
    if path.exists(target_path):
        os.remove(path.join(zeal_docset_dir, docset_dirname))


# cli handlers
# =============================================================================
def list_docsets(installed=False):
    if installed:
        data_manager = ZealotDM()
        if len(data_manager) == 0:
            print("There aren't any known user contributed docsets installed.")
        else:
            print_header("Installed user contributed docsets")
            print("{:30}{}".format("Docset Name", "Version"))
            print("-" * 80)
            for docset_name, contents in \
                    data_manager.get_installed_docsets().items():
                print("{:30}{}".format(docset_name, contents['version']))
            print()
    else:
        print_header("Available user contributed docsets")
        print("{:30}{}".format('Docset Name', 'Version'))
        print("-" * 80)
        for docset, val in get_docset_index_contents().items():
            print("{:30}{}".format(docset.lower(), val['version']))


def install_docset(name):
    name, match = name.lower(), None
    data_manager = ZealotDM()
    if name in data_manager:
        print("{} -- docset is already installed!".format(name))
        sys.exit(0)
    available_docsets = get_docset_index_contents()
    for docset in available_docsets:
        if name == docset.lower():
            match = docset
            break
    print_header("Install docset")
    if match is not None:
        docset_version = available_docsets[match]['version']
        fetch_docset(match, available_docsets[match])
        docset_dirname = unpackage_docset(available_docsets[match])
        data_manager.add_docset(match, docset_dirname, docset_version)
        create_symlink(docset_dirname)
        print("{} -- was installed".format(name))
    else:
        print("{} -- user contributed docset not found!".format(name))


def remove_docset(name):
    name, data_manager = name.lower(), ZealotDM()
    print_header("Remove docset")
    if name in data_manager:
        docset_path = data_manager.get_docset_info(name)['path']
        data_manager.remove_docset(name)
        remove_symlink(docset_path)
        shutil.rmtree(docset_path)
        print("{} -- removed from the installed docsets".format(name))
    else:
        print("{} -- is not installed".format(name))


def update_docsets():
    # need to get list of installed docsets
    # need to get list of docsets from index
    # compare versions?
    # remove->install? or backup current docset and then do a fresh install?
    pass


def main():
    app_init()
    args = vars(parser.parse_args())
    arg_map = {
        'list': list_docsets,
        'install': install_docset,
        'remove': remove_docset,
        'update': update_docsets
    }
    target = args.pop('subparser_name')
    if len(args) > 0:
        arg_map[target](**args)
    else:
        parser.print_help()

if __name__ == "__main__":
    main()
